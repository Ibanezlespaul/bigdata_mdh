# BigData_MDH

The jupyter notebook was used for creating the report.
The different model trained in the report were adjusted in the model-definition part of the notebook, very easy to change.


To run this jupyter notebook I suggest the following installations.

Using windows
Get Anaconda and install it
https://www.anaconda.com/distribution/

Create an virtual environment for conda

Install PySpark in your activated conda environment
conda install pyspark
add destinaton of spark-bin-hadoop\bin folder to your PATH env variable
also add a system variable SPARK_HOME pointing to the spark-bin-hadoop folder


For using Keras, non GPU version. simply install
conda install keras

For scikit
conda install scikit-learn




